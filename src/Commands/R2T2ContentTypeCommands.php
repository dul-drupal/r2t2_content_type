<?php

namespace Drupal\r2t2_content_type\Commands;

use Drush\Commands\DrushCommands;
use Drush\Drush;
use Symfony\Component\Yaml\Yaml;
use Drupal\taxonomy\Entity\Term;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\Entity\FieldConfig;

/**
 * A Drush commandfile
 *
 */
class R2T2ContentTypeCommands extends DrushCommands {
  function termLookup($term, $vid, $return_one = TRUE) {
    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties(['vid' => $vid, 'name' => $term]);
    
    if (count($terms)) {
      return $return_one ? array_key_first($terms) : array_keys($terms);
    }

    return FALSE;
  }

  /**
   * Create user accounts for Web Editorial Board (WEB)
   * members (DEPRECATED)
   * 
   * @usage r2t2-create-Users
   * @command r2t2:createUsers
   * @aliases r2t2-create-users,r2-users
   */
  public function createUsers($options = ['option-name' => 'default']) {
    // return samlauth module to be enabled
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('samlauth') == FALSE) {
      return;
    }

    $externalAuth = \Drupal::service('externalauth.externalauth');

    $dataDir = \Drupal::service('extension.list.module')->getPath('r2t2_content_type') . '/data';
    $csvData = file_get_contents($dataDir . '/users/users.csv');
    $users = explode(PHP_EOL, $csvData);
    $this->logger()->notice(print_r($users, TRUE));
    foreach ($users as $user) {
      if ($user == '') {
        continue;
      }
      list($first_name, $last_name, $name, $role) = explode(',', $user);
      if (!$name) {
        continue;
      }
      try {
        $user = $externalAuth->register($name . "@duke.edu", 'samlauth', ['name' => $name]);
        if ($user != NULL) {
          //$user->set('first_name', $first_name);
          //$user->set('last_name', $last_name);
          $user->addRole($role);
          $user->save();
          $this->logger()->success(sprintf("Assigned '%s' role to user: '%s %s' (%s)...", $role, $first_name, $last_name, $name));
        //} else {
        //  $this->logger()->error(sprintf("Unable to register %s@duke.edu...", $name));
        }
      } catch (Exception $e) {
        $this->logger()->error(sprintf("Unable to register %s@duke.edu...", $name));
      }
    }
  }

  /**
   * Create sample Transparency Project nodes
   *
   * @usage r2t2-create-projects
   * @command r2t2:createProjects
   * @aliases r2t2-create-projects,r2-projects
   */
  public function createProjects($options = ['parent_nid' => '', 
                                             'target-status' => 'In discussion',
                                             'how-many' => 4]) {
    // in the absence of a target "recommendation id" (rid), 
    // select the first recommendation item that has a status 
    // of "In discussion"

    if ($options['parent_nid']) {
      $parent_node_id = $options['parent_nid'];
    } else {
      $query = \Drupal::entityQuery('node');

      $node_ids = $query->condition('type', 'report_recommendation')
                        ->condition('field_recommendation_status', $options['target-status'])
                        ->condition('status', 1)
                        ->range(0, 1)
                        ->accessCheck(FALSE)
                        ->execute();

      if (!$node_ids) {
        $this->logger()->warning("For some reason, I wasn't able to discover '" . $options['target-status'] . "' Report Recommendations. Exiting...");
        return;
      }

      $node_ids = array_values($node_ids);
      $parent_node_id = $node_ids[0];
    }

    $recommendation = Node::load( $parent_node_id );

    $this->logger()->warning(sprintf("Creating %d Transparency Projects for '%s'...", $options['how-many'], $recommendation->getTitle()));

    foreach (range(1, 4) as $number) {
      \Drupal::entityTypeManager()->getStorage('node')->create([
        'type' => 'transparency_project',
        'title' => t(sprintf("New Sample Project #%s", $number)),
        'field_parent_recommendation' => $recommendation->id(),
      ])->save();
      $this->logger()->success("Created project #" . $number . "...");
    }
    $this->logger()->success("Done.");
  }

  /**
   * Import data from remote r2t2 server.
   *
   * @param $filename
   *   Relative (to module/data) or absolute filename path
   * @param array $options
   *   An associative array of options from CLI
   * @usage r2t2-importJsonData
   *
   * @command r2t2:importJsonData
   * @aliases import-r2t2-items
   */
  public function importJsonData($filename = '', $options = ['content-type' => 'report_recommendation', 
                                                             'rows' => 10,
                                                             'all' => FALSE,
                                                             'remove-existing' => FALSE,
                                                             'fix-bodytext' => TRUE,
                                                             'show-responsibles' => FALSE,
                                                             'dry-run' => FALSE]) {
    $dataDir = \Drupal::service('extension.list.module')->getPath('r2t2_content_type') . '/data';
    if ( !$filename ) {
      // assume our 'r2t2_current.json' file
      $filename = $dataDir . '/r2t2_current.json';
    } else {
      if (preg_match("/^\//", $filename) == 0) {
        $filename = $dataDir . '/' . $filename;
      }
      // otherwise, assume absolute file system path.
    }

    if ( $options['remove-existing'] ) {
      # TIP SOURCE:
      # https://drupal.stackexchange.com/questions/537/how-to-delete-all-nodes-of-a-given-content-type
      $this->logger()->notice( "Removing existing 'report_recommendation' nodes...");
      $storage_handler = \Drupal::entityTypeManager()->getStorage("node");
      $entities = $storage_handler->loadByProperties(["type" => $options['content-type']]);
      $storage_handler->delete($entities);
    }

    $yaml = NULL;
    $adjustments = NULL;
    if ( $options['fix-bodytext'] ) {
      $yaml = new \Symfony\Component\Yaml\Yaml();
      // We're going to grossly presume the location of the 
      // 'adjustments' file
      $adjustments_filepath = $dataDir . '/adjustments.yaml';
      $yamlStruct = file_get_contents( $adjustments_filepath );
      $adjustments = $yaml->parse( $yamlStruct );
    }

    // Decode as an associative array.
    // (json_decode returns 'stdClass' by default)
    $fileContents = file_get_contents($filename);
    //$items = json_decode($fileContents, TRUE, 512, JSON_THROW_ON_ERROR);
    $data = json_decode($fileContents, TRUE, 512, JSON_THROW_ON_ERROR);
    $items = $data['recommendations'];

    $this->logger()->notice( count($items) );

    $rows = $options['all'] ? count($items) : $options['rows'];
    $this->logger()->notice( "importing " . $rows . " row[s]..." );

    $i = 0;
    while ( $i < $rows && $items[$i] ) {
      $node = Node::create(['type' => $options['content-type']]);
      $node->langcode = 'en';

      $body = $items[$i]['body'];

      $replace_count = 0;
      if ( $adjustments ) {
        $body = str_replace($adjustments['body']['search'], $adjustments['body']['replace'], $body, $replace_count);
        error_log( sprintf("str_replace count = [%s]", $replace_count) );
      }
      $node->body = ['value' => $body, 'format' => 'full_html'];

      // Get the "Recommendation" source
      $node->field_source = $items[$i]['source'];

      $recommendation_progress = $items[$i]['recommendation_progress'];
      if ($adjustments ) {
        $recommendation_progress = str_replace($adjustments['body']['search'], $adjustments['body']['replace'], 
          $recommendation_progress);
      }
      $node->field_recommendation_progress = $recommendation_progress;

      // For the "Category" field, we need to 
      // * split the string (potentially) by the comma character,
      // * "str_replace" each slug
      // * query for the term's ID (tid)
      // * create array of discovered TIDs and assign to field.
      $this->logger()->notice( "processing the item's category/categories..." );
      $category = $items[$i]['recommendation_category'];
      $slugs = str_replace(
        $adjustments['category']['search'], 
        $adjustments['category']['replace'], 
        explode(', ', $items[$i]['recommendation_category'])
      );
      $tids = array();
      foreach ($slugs as $slug) {
        if ($slug == '') {
          continue;
        }
        $tid = $this->termLookup($slug, 'report_recommendation_category');
        if ($tid !== FALSE) {
          array_push($tids, $tid);
        }
      }
      //$this->logger()->notice( "[Category] Discovered these Term IDs for this item" );
      //$this->logger()->notice( print_r( $tids, TRUE ));
      $node->field_recommendation_category = $tids;

      $this->logger()->notice( "processing the item's responsible parties..." );
      $responsible_parties = str_replace('?', '', $items[$i]['responsible_party']);
      $arr_responsible_parties = preg_split("/[,]\s?/", $responsible_parties);
      $slugs = str_replace(
        $adjustments['responsible_party']['search'],
        $adjustments['responsible_party']['replace'],
        preg_split("/[,]\s?/", $responsible_parties)
      );
      if ($options['show-responsibles']) {
        $this->logger()->notice( 'Responsible parties for recommendation: ' . $items[$i]['title'] );
        $this->logger()->notice( print_r($arr_responsible_parties, TRUE) . "\n" );
      }
      $tids = array();
      foreach ($slugs as $slug) {
        if ($slug == '') {
          continue;
        }
        $tid = $this->termLookup($slug, 'recommendation_responsible_group');
        if ($tid !== FALSE) {
          array_push($tids, $tid);
        }
      }
      $node->field_responsible_party = $tids;

      $node->field_recommendation_status = $items[$i]['current_status'];
      $node->field_eg_decision = $items[$i]['eg_decision'];
      $node->field_campus_partnerships = $items[$i]['campus_partnerships'];
      $node->field_recommendation_date = $items[$i]['recommendation_date'];
      $node->promote = 0;
      $node->uid = 1;
      $node->setPublished( true );
      $node->title = $items[$i]['title'];
      if ($options['dry-run'] == FALSE) {
        $node->save();
        $this->logger()->notice("node id = [" . $node->id() . "]");
        if ($replace_count > 0) {
          $this->logger()->notice( sprintf("Replaced %s instance(s) of bad chars in body field for node: nid=%s", $replace_count, $node->id()) );
        }
      }
      $i++;
    }
  }

  /**
   * Discover categories from R2T2 legacy data
   *
   * @param $field
   *   categories, responsible-party or eg-decisions
   * @param array $options
   *   An associative array of options from CLI
   * @usage r2t2-discoverFieldValues
   *
   * @command r2t2:field-values
   * @aliases r2t2-field-values,r2t2-values
   */
  public function discoverFieldValues( $field = '', $options = ['filename' => 'r2t2_current.json'] ) {
    $map = array(
      'categories' => ['label' => 'categories', 'index' => 3],
      'responsible-party' => ['label' => 'responsible parties', 'index' => 5],
      'eg-decision' => ['label' => 'EG decisions', 'index' => 4],
    );

    if (!key_exists($field, $map)) {
      $this->logger->error("Please provide a valid field (categories, responsible-party, or eg-decision)");
      return;
    }
    $field = $map[$field];
 
    $dataDir = \Drupal::service('extension.list.module')->getPath('r2t2_content_type') . '/data';
    $filename = $options['filename'];
    if (preg_match("/^\//", $filename) == 0) {
      $filename = $dataDir . '/' . $filename;
    }
    $this->logger()->notice( "filename = [" . $filename . "]" );
    
    $fileContents = file_get_contents($filename);
    $items = json_decode($fileContents, TRUE, 512, JSON_THROW_ON_ERROR);

    $this->logger()->notice( count($items) );

    $this->logger()->notice( "importing data now" );
    $this->logger()->notice( "Discovering distinct " . $field['label'] . ". Please wait..." );

    $distincts = [];
    foreach ($items as $item) {
      if (!isset( $distincts[$item[$field['index']] ])) {
        $distincts[$item[$field['index']]] = 0;
      }
      $distincts[$item[$field['index']]]++;
    }
    $this->logger()->notice( print_r( array_keys($distincts), TRUE ));
    $this->logger()->notice( "done" );
  }

}
