<?php

namespace Drupal\r2t2_content_type\Controller\Reports;

use Drupal\Core\Controller\ControllerBase;

class ReportsController extends ControllerBase {
  /**
   * Display a simple, trivial index page
   */
  public function indexPage() {
    $page = [];
    $page['Hello World'] = [
      '#markup' => 'Reporting <em>Hello World</em>.'
    ];
    $page['Meme'] = [
      '#markup' => '<div><img src="https://www.radicalcompliance.com/wp-content/uploads/2020/02/meme-cat-reporting.jpg"/></div>'
    ];
    return $page;
  }
}
