<?php

namespace Drupal\r2t2_content_type\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\node\Entity\NodeType;
use Drupal\node\NodeInterface;

class R2t2ContentTypeController extends ControllerBase {

  public function frontPage() {
    $isUserAuthenticated = \Drupal::currentUser()->isAuthenticated();

    $enterSiteUrl = Url::fromUri('internal:' . ($isUserAuthenticated ? '/recommendations' : '/saml/login'));

    $build = [
      '#markup' => '
        <div class="row justify-content-center">
          <div class="col-12 col-lg-6">
            <div class="card mx-auto my-5 text-center shadow">
              <img class="card-img-top" src="/themes/contrib/vivid_theme/images/DEI-Header-2023-draft01.jpg" alt="...">
              <div class="card-body">
                <h3 class="card-title display-3">
                    Welcome
                </h3>
                <p class="card-text lead">
                  The Transparency Tool tracks Duke University Libraries’ actions, initiatives, and progress in anti-racism and DEIA work from 2018 to 2023. The goal of the Tool is to provide transparency and accountability to staff and stakeholders.
                </p>
                <p class="card-text lead">
                The Libraries published a new <a href="https://library.duke.edu/about/strategic-plan">Strategic Plan</a> in 2024, which incorporates DEIA as a central Pillar. From 2024 on, new initiatives around DEIA are no longer tracked in the Transparency Tool. Once a tracking method has been determined and progress updated, it will be linked to from this page.
                </p>
                <p class="mb-1">
                    <a class="btn btn-primary" href="' . $enterSiteUrl->toString() . '">Enter Site</a>
                </p>
                <p class="small fw-light text-muted">
                  (DUL staff only)
                </p>
              </div>
            </div>
          </div>
        </div>
      '
    ,];

    return $build;
  }

  /**
   * Display entity form for a new Transparency Project
   * whose Parent Recommendation is filled.
   */
  public function newTransparencyProject(NodeInterface $recommendation_node) {
    error_log($recommendation_node->bundle());
    if ($recommendation_node->bundle() != 'report_recommendation') {
      // Return a 404 response since the node isn't a "report_recommendation"
      throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
    }

    // SOURCE TIP
    // https://drupal.stackexchange.com/questions/216480/how-do-i-programmatically-generate-an-entity-form
    $node_type = NodeType::load('transparency_project');

    $node = $this->entityTypeManager()->getStorage('node')->create([
      'type' => $node_type->id(),
      'title' => t('New Untitled Project'),
      'field_parent_recommendation' => $recommendation_node->id()
    ]);
    // $node->save();
    // return $this->redirect('entity.node.canonical', ['node' => $node->id()]);

    $content = \Drupal::service('entity.form_builder')->getForm($node);
    return $content;
  }
}
