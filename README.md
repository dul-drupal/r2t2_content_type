# R2T2 Content Type

## Install In Your Drupal Project

First, tell `composer` where the project is located:  
```bash
$ composer config repositories.dul-drupal/r2t2_content_type git \
    https://gitlab.oit.duke.edu:dul-drupal/r2t2_content_type.git
```
  
**Then**,  
```bash
$ composer require dul-drupal/r2t2_content_type:dev-main
```
### If You're Using a Docker Image...
We typical run Drupal images from Bitnami (`bitnami/drupal`), so with that in mind:  
  
**`Dockerfile`**:  
```Dockerfile
FROM bitnami/drupal:latest

RUN install_packages vim git npm tar rsync wget curl

USER 0

# You'll need to create this folder, otherwise
# you won't be able to 'composer require <anything>'
# because of file permissions.
# Bitnami's images run as non-root.

RUN mkdir -p /.composer/cache/vcs
  && chmod -R a+rw /.composer/cache

# Bitnami images will run scripts under this directory
WORKDIR /docker-entrypoint-init.d
COPY docker-entrypoint-init.d ./
RUN chmod -R a+x /docker-entrypoint-init.d/*

# Other image building stuff

USER 1001

ENTRYPOINT [...]
```
  
**Next**, you'll need to setup a `docker-entrypoint-init.d` directory at your 
project's root (see how we setup `dul-r2t2`), and create a shell script 
to install the module...
  
**/path/to/projroot/docker-entrypoint-init.d/00-install-our-stuff.sh**:  
```bash
#!/bin/sh

set -e

# Remember, this is where Drpual root lives on a Bitnami image
cd /opt/bitnami/drupal

composer config repositories.dul-drupal/r2t2_content_type git \
  https://gitlab.oit.duke.edu:dul-drupal/r2t2_content_type.git

composer require dul-drupal/r2t2_content_type:dev-main

# require other packages if you like

exec "$@"
```
  
The above `Dockerfile` and `docker-entrypoint-init.d` example setup has been tested in a Kubernetes setup and works 
correctly.
  

